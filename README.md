Dépôt "de secours" pour le TP du M1 ASA
=======================================
**Session Rstudio distante** : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/f-santos%2Fbinder-tp-m1-asa/HEAD)

Cé dépôt permettra de réaliser directement en ligne les analyses du TP, pour les étudiants n'ayant pas pu installer correctement les logiciels sur leur poste.

Rappel du site web du TP : [https://f-santos.gitlab.io/tp-master-1-bgs/](https://f-santos.gitlab.io/tp-master-1-bgs/).
